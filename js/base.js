/**
 * Created with IntelliJ IDEA.
 * User: dick
 * Date: 6/29/13
 * Time: 7:13 PM
 * To change this template use File | Settings | File Templates.
 */

(function ($) {
    Slider = function () {
        this.slides = ["lion-wolf", "task", "problem"];

    };

    Slider.prototype = {
        init: function () {
            this.currentSlide = "lion-wolf";
            var next = $("#next");
            var prev = $("#prev");
            var self = this;
            next.on("click", function () {
                self.next();
            });
            prev.on("click", function () {
                self.prev();
            });
        },
        next: function () {
            console.log("next");

            var lionwolf = $("#lion-wolf");
            var task = $("#task");
            var problem = $("#problem");
            var problem2 = $("#problem-2");
            var social = $("#social");
            var group = $("#group");
            var insite = $("#insite");
            var art = $("#art");
            var final = $("#final");
            var banners = $("#banners");
            var banners2 = $("#banners2");
            var banners3 = $("#banners3");
            var bd = $("body");
            var canvas = $("#canvas");
            if (this.currentSlide == "lion-wolf") {
                this.currentSlide = "task";
                lionwolf.css("opacity", "0.1");
                task.css("transform", "translate(-50%, -50%) translate3d(0px, 7000px, 13200px) rotateX(-90deg) rotateY(0deg) rotateZ(0deg) scale(3)");
                task.css("opacity", "1");
                canvas.css("transform", "rotateZ(0deg) rotateY(0deg) rotateX(90deg) translate3d(0px, -14000px, -13500px)");

            } else if (this.currentSlide == "task") {
                this.currentSlide = "problem";
                canvas.css("transform", "rotateZ(90deg) rotateY(0deg) rotateX(90deg) translate3d(0px, -14000px, -15500px)");
                problem.animate({ opacity: 1, left: '50%'}, 1000);
            } else if (this.currentSlide == "problem") {
                this.currentSlide = "image";
                canvas.css("transform", "rotateZ(90deg) rotateY(0deg) rotateX(90deg) translate3d(0px, -14000px, -18500px)");
                problem.animate({top:'200%'});
                social.animate({top:'200px'}, 1000);
            } else if (this.currentSlide == "image") {
                this.currentSlide = "group";
                social.animate({top:'200%', left:'200%'}, 1000);
                group.animate({top: '200px', left:'50%'}, 1000);
            } else if (this.currentSlide == "group") {
                this.currentSlide = "insite";
                group.animate({opacity:0}, 1000);
                insite.animate({opacity:1}, 1000);
            } else if (this.currentSlide == "insite") {
                this.currentSlide = "art";
                art.animate({opacity:1}, 1000);
                insite.animate({opacity:0}, 1000);
            } else if (this.currentSlide == "art") {
                this.currentSlide = "final";
                final.animate({opacity:1}, 1000);
                art.animate({opacity:0}, 1000);
            } else if (this.currentSlide == "final") {
                this.currentSlide = "banners";
                bd.css("background-color", "#000000");
                banners.animate({opacity:1}, 1000);
                final.animate({opacity:0}, 200);
            } else if (this.currentSlide == "banners") {
                this.currentSlide = "banners2";
                banners2.animate({opacity:1}, 1000);
                banners.animate({opacity:0}, 200);
            } else if (this.currentSlide == "banners2") {
                this.currentSlide = "banners3";
                banners3.animate({opacity:1}, 1000);
                banners2.animate({opacity:0}, 200);
            } else if (this.currentSlide == "banners3") {
                banners3.animate({opacity:0}, 1000);
                problem.remove();
                canvas.remove();
                $("<div class='row-fluid'><div class='span2'></div><div class='span8'><img src='img/site.png'></div><div class='span2'></div></div>").appendTo("body");
                $("body").css("overflow-y", "auto");
                $("body").css("padding-top", "40px");
            }


        },
        prev: function () {
            console.log("prev");
        }

    }
    return Slider;
})($);
var slider = new Slider;
slider.init();
